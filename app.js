var isCaculated = false;

function OnCButtonClick() {
    if (isCaculated) {
        document.getElementById("screen").value = "";
        isCaculated = false;
    }
    var oldText = document.getElementById("screen").value;
    var newText = oldText.substring(0, oldText.length - 1);
    document.getElementById("screen").value = newText;
}

function OnNormalButtonClick(input) {
    if (isCaculated) {
        document.getElementById("screen").value = "";
        isCaculated = false;
    }
    switch (input) {
        case "×":
            document.getElementById("screen").value += "*";
            break;
        case "÷":
            document.getElementById("screen").value += "/";
            break;
        default:
            document.getElementById("screen").value += input;
            break;
    }
}

function OnCaculateButtonClick() {
    isCaculated = true;
    try {
        document.getElementById("screen").value = eval(document.getElementById("screen").value);
    }
    catch (ex) {
        alert(ex);
    }
}